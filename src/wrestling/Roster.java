/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wrestling;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.swing.JOptionPane;

/**
 *
 * @author Eric Price
 */
public class Roster
{
    ArrayList<Wrestler> roster = new ArrayList<Wrestler>();
    public int CurrentRoster;
    
    public Roster()
    {
        CurrentRoster = 0;
    }
    
    public void AddWrestler(Wrestler Jobber)
    {
        roster.add(Jobber);
        CurrentRoster++;
    }
    
    public void sortByName()
    {
        
    }
    
    public void sortByOverall()
    {
        
    }
    
    public void sortByPopularity()
    {
        
    }
    
    public void ShowRoster()
    {
        String DisplayRoster = new String();
        Collections.sort(roster);
        
        DisplayRoster = "The current roster is" + "\n" +"(Name/Popularity/Overall)" + "\n";
        for(int i = 0; i < CurrentRoster; i++)
        {
            DisplayRoster = DisplayRoster + roster.get(i);
        }
        JOptionPane.showMessageDialog(null, DisplayRoster);
    }
    
}
