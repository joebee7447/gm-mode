/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wrestling;

/**
 *
 * @author jtbum
 */
public class Email 
{
    private String subjectLine;
    private String description;
    
    public Email getInjuryEmail(Wrestler w)
    {
        this.subjectLine = w.getName() + " Injured!";
        this.description = w.getName() + " has been injured! They are out for " + w.getWeeksOut() + " weeks.";
        return this;
    }
    
    public Email getMoneyEmail()
    {
        this.subjectLine = "Low on Money!";
        this.description = "You're running very low on money. Release superstars if you have to.";
        return this;
    }
    
    public Email getRosterEmail()
    {
        this.subjectLine = "Your roster is too small!";
        this.description = "You need to hire more wrestlers!";
        return this;
    }
    
    public Email getTradeAcceptedEmail()
    {
        this.subjectLine = "Trade Accepted";
        this.description = "Your trade has been accepted!";
        return this;
    }
    
    public Email getTradeDeclinedEmail()
    {
        this.subjectLine = "Trade Declined!";
        this.description = "Your trade has been declined!";
        return this;
    }
    
}
