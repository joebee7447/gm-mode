package wrestling;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;







public class Wrestling {
     
    public static void main(String[] args) 
    {
        Roster Raw = new Roster();
        Roster SmackDown = new Roster();
        Roster NXT = new Roster();
        
        int UserChoice1 = 0;
        int UserChoice2 = 0;
        
        Wrestler AJ = new Wrestler("AJ Styles", false,  100, 90, 100, 80, 10, 55, 76);
        Wrestler Brock = new Wrestler("Brock Lesner", false, 90, 100, 100, 100, 100, 100, 100);
        Wrestler Gargano = new Wrestler("Johnny Wrestling", false, 80, 10, 100, 100, 30, 100, 100);
        Wrestler KO = new Wrestler("Kevin Owens", false,  70, 100, 100, 10, 100, 100, 100);
        Wrestler sami = new Wrestler("Sami Zayn", false, 60, 100, 70, 60, 100, 100, 100);
        Wrestler rko = new Wrestler("Randy Orton", false, 50, 100, 10, 10, 100, 100, 100);
        Wrestler show = new Wrestler("Big Show", false,  40, 100, 100, 100, 100, 100, 10);
        Wrestler y2j = new Wrestler("Chris Jericho", false, 30, 100, 100, 100, 10, 10, 10);
        Wrestler Rob = new Wrestler("Rob Gronkowski", false, 20, 100, 100, 40, 100, 100, 100);
        
        SmackDown.AddWrestler(AJ);
        Raw.AddWrestler(Brock);
        NXT.AddWrestler(Gargano);
        SmackDown.AddWrestler(KO);
        Raw.AddWrestler(sami);
        NXT.AddWrestler(rko);
        SmackDown.AddWrestler(show);
        Raw.AddWrestler(y2j);
        NXT.AddWrestler(Rob);
        
        while (UserChoice1 < 3)
        {      
        UserChoice1 = Integer.parseInt(JOptionPane.showInputDialog("Enter 1 for Create Wrestler or Enter 2 for Show Roster or 3 to exit"));
            if (UserChoice1 == 1)
             {
                Wrestler Jobber = new Wrestler();
                Jobber.CreateWrestler();
                UserChoice2 = Jobber.ChooseAShow();
                
                if (UserChoice2 == 1){
                    Raw.AddWrestler(Jobber);
                }
                else if (UserChoice2 == 2){
                    SmackDown.AddWrestler(Jobber);
                }
                else{
                    NXT.AddWrestler(Jobber);
                }
             }
            else if (UserChoice1 == 2)
            {
                JOptionPane.showMessageDialog(null, "Raw Roster");
                Raw.ShowRoster();
                JOptionPane.showMessageDialog(null, "SmackDown Roster");
                SmackDown.ShowRoster();
                JOptionPane.showMessageDialog(null, "NXT Roster");
                NXT.ShowRoster();
                
            }
        }

    }

}