/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wrestling;

import java.util.List;
import javax.swing.JOptionPane;
import java.util.Arrays;
/**
 *
 * @author Eric Price
 */
public class Wrestler implements Comparable<Wrestler>
{
        private String name = "";               //empty string containing wrestler name
        private boolean isfemale = false;       //boolean to determine female wrestlers
        private int popularity, strength, agility, charisma, stamina, hardcore, durability, overall = 0;    //initializes all private nums to 0
        private int weeksRemaining = 0;
        private boolean isInjured = false;
        private int recoveryTime = 0;
        private int wins, losses = 0;
        public Wrestler()
        {
            
        }
        
        public Wrestler(String name, Boolean isfemale, int popularity, int strength, int agility, int charisma, int stamina, int hardcore, int durability) 
        {
            //creates wrestler with passed attributes
            this.name = name;
            this.isfemale = isfemale;
            this.popularity = popularity;
            this.strength = strength;
            this.agility = agility;
            this.charisma = charisma;
            this.stamina = stamina;
            this.hardcore = hardcore;
            this.durability = durability;
            setOverall();
            
        }
        public Wrestler(Wrestler r)
        {
            //copy constructor
            this.name = r.name;
            this.isfemale = r.isfemale;
            this.popularity = r.popularity;
            this.strength = r.strength;
            this.agility = r.agility;
            this.charisma = r.charisma;
            this.stamina = r.stamina;
            this.hardcore = r.hardcore;
            this.durability = r.durability;
            this.overall = r.overall;
            
        }
        public String getName()                 //set and get methods for each attribute
        {
            return name;
        }
        
        public void setName(String n)                 //set and get methods for each attribute
        {
            name = n;
        }
        public int getPop()                 //set and get methods for each attribute
        {
            return popularity;
        }
        
        public void setPop(int p)
        {
            popularity = p;
        }
        
        public int getStrength()
        {
            return strength;
        }
        
        public void setStrength(int s)
        {
            strength = s;
        }
        
        public int getAgility()
        {
            return agility;
        }
        
        public void setAgility(int a)
        {
            agility = a;
        }
        
        public int getHardcore()
        {
            return hardcore;
        }
        
        public void setHardcore(int h)
        {
            hardcore = h;
        }
        
        public int getCharisma()
        {
            return charisma;
        }
        
        public void setCharisma(int c)
        {
            charisma = c;
        }
        
        public int getStamina()
        {
            return stamina;
        }
        
        public void setStamina(int s)
        {
            stamina = s;
        }
        
        public int getDurability()
        {
            return durability;
        }
        
        public void setDurability(int d)
        {
            durability = d;
        }
        
        public int getOverall()
        {
            return overall;
        }
        
        public void setOverall()
        {
            overall = (agility + charisma + durability + hardcore + stamina + strength) / 6;
        }
        
        public int getWeeksRemaining()
        {
            return weeksRemaining;
        }
        
        public void setWeeksRemaining(int contract)
        {
            weeksRemaining = contract;
        }
        
        public boolean getInjury()
        {
            return isInjured;
        }
        
        public int getWeeksOut()
        {
            return recoveryTime;
        }
        
        public void setInjury(int timeOut)
        {
            isInjured = true;
            recoveryTime = timeOut;
        }
        
        public void updateInjury()
        {
            if(recoveryTime > 0)
            {
                recoveryTime -= 1;
            }
            
            if(recoveryTime == 0)
            {
                isInjured = false;
            }
        }
        
        public void CreateWrestler()
        {
            name = JOptionPane.showInputDialog("Enter name");
            isfemale = false;
            popularity = Integer.parseInt(JOptionPane.showInputDialog("Enter popularity (0-100)"));
            agility = Integer.parseInt(JOptionPane.showInputDialog("Enter agility (0-100)"));
            charisma = Integer.parseInt(JOptionPane.showInputDialog("Enter charisma (0-100)"));
            durability = Integer.parseInt(JOptionPane.showInputDialog("Enter durability (0-100)"));
            hardcore = Integer.parseInt(JOptionPane.showInputDialog("Enter hardcore (0-100)"));
            stamina = Integer.parseInt(JOptionPane.showInputDialog("Enter stamina (0-100)"));
            strength = Integer.parseInt(JOptionPane.showInputDialog("Enter strength (0-100)"));
            setOverall();

        }
    
        public int compareTo(Wrestler w)
        {
            return w.getPop() - this.getPop();
        }

        public void updateWins()
        {
            wins++;
        }
        
        public void updateLosses()
        {
            losses++;
        }
        
        public int getWins()
        {
            return wins;        
        }
        
        public int getLosses()
        {
            return losses;
        }
        
        public int ChooseAShow()
        {
            return (Integer.parseInt(JOptionPane.showInputDialog("Choose a Show" + "\n" + "1 for Raw, 2 for SmackDown, 3 for NXT")));
        }
        
        public String toString()
        {
            return (name + "     " + popularity + "      " + overall + "\n");
        }
}
