/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wrestling;

import java.util.ArrayList;

/**
 *
 * @author jtbum
 */
public class Show 
{
    private String name, shortName;
    private ArrayList<Email> inbox = new ArrayList<Email>();
    private Roster myRoster;
    private int fans;
    private int money;
    
    public void injuryEmail(Wrestler w)
    {
        Email e = new Email();
        e.getInjuryEmail(w);
        inbox.add(e);
    }
    
    public void weeklyEmail()
    {
        if(money < 1000)
        {
            Email e = new Email();
            e.getMoneyEmail();
            inbox.add(e);
        }
        
        if(myRoster.length < 1000)
        {
            Email e = new Email();
            e.getMoneyEmail();
            inbox.add(e);
        }
    }
}
